namespace :utils do
    desc "Popular banco de dados."
    task seed: :environment do
      puts "Gerando os contatos (Contacts)..."
        100.times do |i|
          Contato.create!(
            name: Faker::Name.name,
            email: Faker::Internet.email,
            phone:Faker::PhoneNumber
  )
        end
    end
end