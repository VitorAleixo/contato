class V1::ContatosController < ApplicationController
    def index
        @contatos = Contato.all

        render json: @contatos, status: :ok
    end

    def show
        @contato = Contato.find(params[:id])
        render json: @contato, status: :ok
    end

    def create 
        @contato = Contato.new(contato_params)

        @contato.save 
        render json: @contato, status: :created
    end
    
    def update
        @contato = Contato.find(params[:id])

        if @contato.update(contato_params)
            render json: @contato, status: :ok
        else 
            render json: { errors: @contato.errors }, status: 422
        end    
    end

    def destroy
        @contato = Contato.find(params[:id])
       if @contato.destroy
        head(:ok)
       else
        head(:unprocessable_entity)
       end

    end

    private

    def contato_params
        params.permit(:url, :description)
    end
end
